import { RedisPubSub } from 'graphql-redis-subscriptions';
import constants from './constants';

export const pubsub = new RedisPubSub({
	connection: {
		host: constants.REDIS_DOMAIN_NAME,
		port: constants.REDIS_PORT_NUMBER,
		password: constants.REDIS_PASSWORD,
		retry_strategy: (options) => {
			return Math.max(options.attempt * 100, 3000);
		}
	}
});
