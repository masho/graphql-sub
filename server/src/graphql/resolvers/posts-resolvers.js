import Post from '../../models/Posts/Post';
import PostComment from '../../models/Posts/PostComment';
import { requireAuth } from '../../services/auth';
import { pubsub } from '../../config/pubsub';
import User from '../../models/User';
import { withFilter } from 'graphql-subscriptions';
import shortid from 'shortid';
import Storage from '@google-cloud/storage';
import { unlink, createWriteStream } from 'fs';
import * as admin from 'firebase-admin';

const NEW_POST = 'new_post';
const POST_COMMENT_ADDED = 'post_comment_added';
const UPLOAD_DIR = './';
let fileURL = '';
const bucket = 'mash';

const serviceAccount = require('../../../pushnot2470-firebase-adminsdk-uyn1n-0aae92e6c2.json');
admin.initializeApp({
	credential: admin.credential.cert(serviceAccount)
});

const regToken =
	'd9iaaAWwBZs:APA91bEFjWZn8AwYgG6Yq85DKSsnWgt86-L7kqwUZj1jX3hhFKj2hS0ti7qVpUh4EoxsRiGk5Q_UyoQkpjTp0TzXGOeAyTW6AzxdB49iZ-5B9JYdO6jMQcg3oMsNZjJbAUAtdSYA7Ia7';

const storage = Storage({
	projectId: 'graph-sub',
	keyFilename: './graph-sub-cf15f3e71de7.json'
});

//Handles upload of file to G-cloud bucket
function uploadFile(bucketName, filename) {
	storage
		.bucket(bucketName)
		.upload(filename, {
			gzip: true,
			public: true
		})
		.then(() => {
			console.log(`${filename} uploaded to ${bucketName} bucket`);
			unlink(filename, (err) => {
				if (err) {
					throw err;
				}
				console.log(`File deleted`);
			});
		})
		.catch((err) => {
			console.error(`ERR: ${err}`);
		});
}

//Handle Storage of Upload
const storeUpload = ({ stream, filename }) => {
	const id = shortid.generate();
	const path = `${UPLOAD_DIR}${id}-${filename}`;
	const absPath = `${id}-${filename}`;
	return new Promise((resolve, reject) =>
		stream.pipe(createWriteStream(path)).on('error', (error) => reject(error)).on('finish', async () => {
			await uploadFile(bucket, path);
			fileURL = `https://storage.googleapis.com/${bucket}/${absPath}`;
			resolve({ fileURL, path });
		})
	);
};

//Processess the Upload
const processUpload = async (upload) => {
	const { stream, filename, mimetype, encoding } = await upload;
	const { fileURL, path } = await storeUpload({ stream, filename });
	let url = fileURL;
	console.log(path);
	return { url, mimetype, encoding, path };
};

export default {
	//Handle file Upload
	file_upload: async (_, { file }, { user }) => {
		return processUpload(file);
	},
	//Creating post by logged in user
	create_post: async (_, { content, sector }, { user }) => {
		try {
			//Authenticating user
			let userInfo = await requireAuth(user);
			//Creating new post
			let new_post = await Post.create({ user: user._id, content: content, sector: sector });
			//New post subscription
			pubsub.publish(NEW_POST, { [NEW_POST]: new_post });

			//FCM
			const message = {
				notification: {
					title: `New Topic in Sector: ${sector}`,
					body: content
				},
				// android: {
				// 	ttl: 3600 * 1000,
				// 	notification: {
				// 		icon: 'stock_ticker_update',
				// 		color: '#f45342',
				// 	},
				// },
				// apns: {
				// 	payload: {
				// 		aps: {
				// 			badge: 42,
				// 		},
				// 	},
				// },
				token: regToken
			};

			admin
				.messaging()
				.send(message)
				.then((response) => {
					console.log('Successfully sent:', response);
				})
				.catch((error) => {
					console.log('Error sending message: ', error);
				});

			return new_post;
		} catch (error) {
			throw error;
		}
	},
	//Getting posts in users department or faculty
	posts: async (_, { cursor, limit }, { user }) => {
		try {
			//Authenticating user
			let userInfo = await requireAuth(user);
			//Checking if cursor is set
			if (cursor != undefined) {
				return await Post.find({ sector: sector }).limit(limit).sort({ createdAt: -1 });
			}
			//If cursor is not provided
			return await Post.find({ sector: sector }).limit(limit).sort({ createdAt: -1 });
		} catch (error) {
			throw error;
		}
	},
	//Getting psot created by logged user
	my_posts: async (_, { cursor, limit }, { user }) => {
		try {
			//Authenticating user
			let userInfo = await requireAuth(user);
			//Checking if cursor is set
			if (cursor != undefined) {
				return await Post.find({ user: user._id }, { createdAt: { $lt: cursor } })
					.limit(limit)
					.sort({ createdAt: -1 });
			}
			return await Post.find({ user: user._id }).limit(limit).sort({ createdAt: -1 });
		} catch (error) {
			throw error;
		}
	},
	//Currently logged in user answering questions
	create_post_comment: async (_, args, { user }) => {
		try {
			//Authenticating user
			await requireAuth(user);
			const comment = await PostComment.create({ user: user._id, ...args });
			if (comment) {
				//Increasing the number of counts of answers by 1
				let updated_post = await Post.findByIdAndUpdate(
					comment.post,
					{ $inc: { no_answers: +1 } },
					{ new: true }
				);
				//Subscription for increase in number post comments
				pubsub.publish(POST_COMMENT_INCREASED, { [POST_COMMENT_INCREASED]: updated_post });
				//Subscription for new post comment
				pubsub.publish(POST_COMMENT_ADDED, { [POST_COMMENT_ADDED]: comment });
				return comment;
			} else {
				throw new Error('An error occured');
			}
		} catch (error) {
			throw error;
		}
	},
	//Getting the last 7 answers
	post_comments: async (_, { post, cursor, limit }, { user }) => {
		try {
			//Validating user
			await requireAuth(user);
			if (cursor) {
				return await PostComment.find({ post: post, createdAt: { $lt: cursor } })
					.limit(limit)
					.sort({ createdAt: -1 });
			}
			return await PostComment.find({ post: post }).limit(limit).sort({ createdAt: -1 });
		} catch (error) {
			throw error;
		}
	},
	/*******************************************************************
   *******************************************************************
    Subscriptions come here
   *******************************************************************
  ********************************************************************/
	//Returns new post
	new_post: {
		subscribe: withFilter(
			() => pubsub.asyncIterator(NEW_POST),
			(payload, variables) => {
				return Boolean(payload.new_post.sector === variables.sector);
			}
		)
	},
	//Returns new post comment
	post_comment_added: {
		subscribe: withFilter(
			() => pubsub.asyncIterator(POST_COMMENT_ADDED),
			(payload, variables) => {
				return Boolean(toString(payload.post_comment_added.post) === toString(variables.post));
			}
		)
	}
};
