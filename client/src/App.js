import React from 'react';
import { Mutation } from 'react-apollo';
import Dropzone from 'react-dropzone';
import gql from 'graphql-tag';

const uplaodFileMutation = gql`
	mutation($file: Upload!) {
		singleUpload(file: $file) {
			url
			encoding
			mimetype
			path
		}
	}
`;

export default () => (
	<Mutation mutation={uplaodFileMutation}>
		{(mutate) => (
			<Dropzone onDrop={([ file ]) => mutate({ variables: { file } })}>
				<p>Drop a file here</p>
			</Dropzone>
		)}
	</Mutation>
);
